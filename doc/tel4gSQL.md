﻿-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- GÃ©nÃ©rÃ© le: Lun 22 Juin 2015 Ã  11:00
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `tel4g` [Lien vers la base de donnée SQL](tel4g.sql)
--

	CREATE DATABASE IF NOT EXISTS `tel4g` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
	USE `tel4g`;

-- --------------------------------------------------------

--
-- Structure de la table `ci_sessions`
--

	CREATE TABLE IF NOT EXISTS `ci_sessions` (
	  `session_id` varchar(40) NOT NULL DEFAULT '0',
	  `ip_address` varchar(16) NOT NULL DEFAULT '0',
	  `user_agent` varchar(120) NOT NULL,
	  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
	  `user_data` text NOT NULL,
	  PRIMARY KEY (`session_id`),
	  KEY `last_activity_idx` (`last_activity`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `offres`
--

	CREATE TABLE IF NOT EXISTS `offres` (
	  `id_offre` int(11) NOT NULL AUTO_INCREMENT,
	  `nom_offre` varchar(50) NOT NULL,
	  `tarif_offre` float NOT NULL,
	  `caract_offre` varchar(500) NOT NULL,
	  `type_offre` int(11) DEFAULT NULL,
	  `active_offre` tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`id_offre`),
	  KEY `type_offre` (`type_offre`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `offres`
--

	INSERT INTO `offres` (`id_offre`, `nom_offre`, `tarif_offre`, `caract_offre`, `type_offre`, `active_offre`) VALUES
	(1, 'Bas prix', 2.49, '2h de communications --Vers fixes et mobiles --Zone nationale', 1, 1),
	(2, 'National', 9.99, 'Téléphonie et SMS illimitÃ©s --Vers fixes et mobiles --Zone nationale', 1, 1),
	(3, 'Euro', 19.99, 'TÃ©lÃ©phonies et SMS illimitÃ©s --Vers fixes et mobiles --Zone EURO', 1, 1),
	(4, 'Data', 5, '3Go', 2, 1),
	(5, 'Data Max', 10, '20Go', 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `phones`
--

	CREATE TABLE IF NOT EXISTS `phones` (
	  `id_phone` int(11) NOT NULL AUTO_INCREMENT,
	  `name_phone` varchar(100) NOT NULL,
	  `img_phone` varchar(200) DEFAULT NULL,
	  `brand_phone` int(11) DEFAULT NULL,
	  `os_phone` int(11) DEFAULT NULL,
	  `ref_phone` varchar(100) NOT NULL,
	  `prix_phone` double NOT NULL,
	  `weight_phone` double NOT NULL,
	  `diag_phone` double NOT NULL,
	  `height_phone` double NOT NULL,
	  `lenght_phone` double NOT NULL,
	  `thick_phone` double NOT NULL,
	  `autonomy_phone` int(11) NOT NULL,
	  `memory_phone` int(11) NOT NULL,
	  `memory_ext_phone` tinyint(1) NOT NULL,
	  `four_g_phone` tinyint(1) NOT NULL,
	  PRIMARY KEY (`id_phone`),
	  KEY `brand_phone` (`brand_phone`,`os_phone`),
	  KEY `brand_phone_2` (`brand_phone`),
	  KEY `os_phone` (`os_phone`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `phones`
--

	INSERT INTO `phones` (`id_phone`, `name_phone`, `img_phone`, `brand_phone`, `os_phone`, `ref_phone`, `prix_phone`, `weight_phone`, `diag_phone`, `height_phone`, `lenght_phone`, `thick_phone`, `autonomy_phone`, `memory_phone`, `memory_ext_phone`, `four_g_phone`) VALUES
	(1, 'Xperia Z3', 'xperia-z3-compact.jpg', 4, 1, 'Xperia Z3', 364, 129, 4.6, 127.3, 64.9, 8.64, 920, 16, 1, 1),
	(2, 'OnePlus', 'oneplus-one.jpg', 5, 1, 'OnePlus', 385, 162, 5.5, 152.9, 75.9, 8.9, 800, 16, 0, 1),
	(3, 'Galaxy Note 3', 'samsung-galaxy-note-3.jpg', 2, 1, 'Galaxy Note 3', 395, 168, 4.3, 151.2, 79.2, 8.3, 800, 32, 1, 1),
	(4, 'HTC One M8', 'htc-one-m8.jpg', 6, 1, 'HTC One M8', 399.89, 160, 5, 146.36, 70.6, 9.35, 720, 16, 1, 1),
	(5, 'Ascend Mate 7', 'huawei-ascend-mate-7.jpg', 7, 1, 'Ascend Mate 7', 405.6, 185, 6, 157, 81, 7.9, 648, 16, 1, 1),
	(6, 'iPhone 6', 'apple-iphone-6.png', 3, 2, 'iPhone 6', 643.07, 129, 4.7, 138.1, 67, 6.9, 250, 16, 0, 1),
	(7, 'iPhone 6 Plus', 'apple-iphone-6-plus.png', 3, 2, 'iPhone 6 Plus', 739, 172, 5.5, 158.1, 77.8, 7.1, 384, 16, 0, 1),
	(8, 'Lumia 735', 'nokia-lumia-730.jpg', 8, 3, 'Lumia 735', 175.41, 150, 4.7, 134.7, 68.5, 8.8, 600, 8, 1, 1),
	(9, 'Lumia 640', 'microsoft-lumia-640.jpg', 8, 3, 'Lumia 640', 169, 145, 5, 141.3, 72.8, 8.8, 620, 8, 1, 1),
	(10, 'G2', 'LG_G2.jpg', 1, 1, 'G2', 289, 141, 5.2, 138.5, 70.9, 8.9, 720, 32, 0, 1),
	(11, 'G3', 'LG_G3.jpg', 1, 1, 'G3', 334.99, 149, 5.5, 146.3, 74.6, 8.9, 800, 32, 1, 1),
	(12, 'Zenfone 2', 'asus-zenfone-2.jpg', 9, 1, 'Zenfone 2', 349, 170, 5.5, 152, 77, 11, 800, 32, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `phones_brand`
--

	CREATE TABLE IF NOT EXISTS `phones_brand` (
	  `id_brand` int(11) NOT NULL AUTO_INCREMENT,
	  `name_brand` varchar(50) NOT NULL,
	  PRIMARY KEY (`id_brand`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `phones_brand`
--

	INSERT INTO `phones_brand` (`id_brand`, `name_brand`) VALUES
	(1, 'LG'),
	(2, 'Samsung'),
	(3, 'Apple'),
	(4, 'Sony'),
	(5, 'One'),
	(6, 'HTC'),
	(7, 'Huawei'),
	(8, 'Nokia'),
	(9, 'Asus');

-- --------------------------------------------------------

--
-- Structure de la table `phones_os`
--

	CREATE TABLE IF NOT EXISTS `phones_os` (
	  `id_os` int(11) NOT NULL AUTO_INCREMENT,
	  `nom_os` varchar(50) NOT NULL,
	  PRIMARY KEY (`id_os`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `phones_os`
--

	INSERT INTO `phones_os` (`id_os`, `nom_os`) VALUES
	(1, 'Android'),
	(2, 'iOS'),
	(3, 'Windows Phone');

-- --------------------------------------------------------

--
-- Structure de la table `type_offre`
--

	CREATE TABLE IF NOT EXISTS `type_offre` (
	  `id_type_offre` int(11) NOT NULL AUTO_INCREMENT,
	  `nom_type_offre` varchar(50) NOT NULL,
	  PRIMARY KEY (`id_type_offre`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `type_offre`
--

	INSERT INTO `type_offre` (`id_type_offre`, `nom_type_offre`) VALUES
	(1, 'abonnements'),
	(2, 'offres');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

	CREATE TABLE IF NOT EXISTS `users` (
	  `id_user` int(11) NOT NULL AUTO_INCREMENT,
	  `nom_user` varchar(50) NOT NULL,
	  `prenom_user` varchar(50) NOT NULL,
	  `date_user` date DEFAULT NULL,
	  `email_user` varchar(70) NOT NULL,
	  `mdp_user` varchar(200) NOT NULL,
	  `type_user` int(11) DEFAULT NULL,
	  PRIMARY KEY (`id_user`),
	  KEY `type_user` (`type_user`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `users`
--

	INSERT INTO `users` (`id_user`, `nom_user`, `prenom_user`, `date_user`, `email_user`, `mdp_user`, `type_user`) VALUES
	(5, 'eddy', 'lainez', '1988-08-26', 'eddy.lainez@gmail.com', 'eddy', NULL),
	(6, 'test', 'test', '2015-06-01', 'test@test.com', 'test', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users_type`
--

	CREATE TABLE IF NOT EXISTS `users_type` (
	  `id_type_user` int(11) NOT NULL AUTO_INCREMENT,
	  `nom_type_user` varchar(50) NOT NULL,
	  PRIMARY KEY (`id_type_user`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `users_type`
--

	INSERT INTO `users_type` (`id_type_user`, `nom_type_user`) VALUES
	(1, 'Administrateur'),
	(2, 'Utilisateur');

--
-- Contraintes pour les tables exportÃ©es
--

--
-- Contraintes pour la table `offres`
--
	ALTER TABLE `offres`
	  ADD CONSTRAINT `ct_type_offre` FOREIGN KEY (`type_offre`) REFERENCES `type_offre` (`id_type_offre`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `phones`
--
	ALTER TABLE `phones`
	  ADD CONSTRAINT `CT_brand` FOREIGN KEY (`brand_phone`) REFERENCES `phones_brand` (`id_brand`) ON DELETE SET NULL ON UPDATE CASCADE,
	  ADD CONSTRAINT `CT_os` FOREIGN KEY (`os_phone`) REFERENCES `phones_os` (`id_os`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
	ALTER TABLE `users`
	  ADD CONSTRAINT `ct_type_user` FOREIGN KEY (`type_user`) REFERENCES `users_type` (`id_type_user`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

