﻿Etape 1: le fichier hosts
-------------------------

Allez chercher le fichier C:\Windows\System32\drivers\etc\hosts et éditez-le.
Ajouter la ligne suivante en bas de fichier : ** 127.0.0.1 local.tel4g **

- - - - - - - - - -
Etape 2: l'hôte virtuel monprojet.dev
-------------------------------------

Vous allez créer un premier virtual host, afin de faire pointer le nom de domaine monprojet.dev sur votre projet.
Pour cela, rendez vous dans le dossier C:\wamp\alias\, et créez un fichier tel4g.conf, dans lequel vous copierez ceci :

	NameVirtualHost local.tel4g
	<VirtualHost local.tel4g>
		DocumentRoot F:/Programme/wamp/www/tel4g
		ServerName local.tel4g
	</VirtualHost>

- - - - - - - - - -
Etape 3: Résoudre le problème de localhost
------------------------------------------

Si vous accédez à http://localhost, ou 127.0.0.1, vous verrez que le virtual host a pris le dessus, et que vous êtes redirigé vers votre projet !

Afin d’éviter cet effet secondaire, vous allez créer un nouveau virtual host, pour localhost. Toujours dans le dossier C:\wamp\alias\, créez un fichier localhost.conf, dans lequel vous copierez ceci :

	NameVirtualHost localhost
	<VirtualHost localhost>
		DocumentRoot F:/Programme/wamp/www/
		ServerName localhost
	</VirtualHost>

Il ne vous reste plus qu'à redémarrer Apache. 

[Source](http://blog.smarchal.com/creer-un-virtualhost-avec-wampserver/) 