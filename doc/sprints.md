﻿Premier Sprint 
==============

US 1: Page d'accueil
--------------------
* Redirection vers la pas "http://local.tel4g/index" et définition du titre de la page;
* Création de la structure de la table des offres et des catégories d'offre (abonnements et options);
* Ajout des données dans la base de données;
* Création de la template qui représentera chaques abonnements et options;
* Récupération des données de la table des offres dans la base de données;
* Mise en page de la page d'accueil.

US 2: Sélection des offres
--------------------------
* Sélection d'une offre sur la page d'accueil;
* Changer l'apparence du pointeur au survole; 
* Mise en surbrillance lors du click; 
* Sélection d'un abonnement et d'une offre simultanément; 
* Impossible de sélectionner plusieurs abonnements/options; 
* Impossible de sélectionner une option sans abonnement sélectionné au préalable; 
* Affichage d'un bouton valider lorsqu'un abonnement est sélectionné; 
* Déselection lors d'un nouveau click sur un abonnement ou une option déjà sélectionné; 
* Faire disparaître le bouton "valider" s'il n'y a plus d'abonnement ou d'option sélectionné.

US 3: Boutique de téléphones
----------------------------
* Redirection vers la page "http://local.tel4g/phones" qui aménèra à la page de présentation de téléphones et dont le titre est "Nos téléphones"; 
* Création de la table "phones" qui contiendra les données des téléphones vendus sur le site; 
* Création du template qui contiendra chaques téléphones; 
* Récupération des données des téléphones pour les insérer dans le template créé précédement; 
* Mise en page; 
* Lors du clique sur un téléphone, redirection vers la page du téléphone cliqué à l'adresse "http://local.tel4g/phones/id" et affichant les données correspondant au téléphone sélectionné. 

US 4: Inscription
-----------------
* Création de la classe user ainsi que la méthode d'inscription;
* Routing de l'adresse http://local.tel4g/user/new vers la méthode précédement créée;
* Création du formulaire d'inscription;
* Vérification des données encodées;
* Création de la table "users" dans la base de données;
* Enregistrement des données dans la table créé.
- - - - - - - - - -

Second Sprint 
=============
US 5: Authentification
----------------------
* Création de la méthode d'authentification;
* Routing de l'adresse http://local.tel4g/authentication vers la méthode créée;
* Création du formulaire d'authentification;
* Vérification de l'adresse mail encodée pour ensuite vérifier le mot de passe;
* Si correct, stockage de l'adresse mail en session, l'utilisateur est alors connecté.

US 6: Bandeau de navigation
---------------------------
* Création d'un template qui reprendra le bandeau de navigation;
* Création des liens;
* Appel de ce template sur chaque page;
* Vérification si la variable de session conservant l'email existe pour savoir si l'on est connecté et donc adapter le bandeau de navigation.

US 7: Connexion & inscription
-----------------------------
* Vérification sur les pages "authentication" et "user/new" si la variable de session conservant l'email existe;
* Si elle existe, on redirige le visiteur;
* A l'inscription d'un nouvel utilisateur, le connecter automatiquement.
- - - - - - - - - -

Troisième Sprint 
================
US 8: Validation de l'abonnement
--------------------------------
* Création du formulaire sur la page d'accueil pour le bouton de validation;
* Création de la class shop et de la méthode abonnement, qui appellera une vue qui affichera le récapitulatif des choix;
* Vérification à l'arrivée sur cette page que l'utilisateur est connecté et stockage de ses choix en session;
* Si pas connecté, on le redirige vers la page de connexion ou d'inscription;
* Une fois connecté, redirection automatique vers la page de validation de l'abonnement;
* Lorsqu'il appui sur le bouton "annuler", il est renvoyé vers la page d'accueil, ses précédents choix sont alors resélectionnés.
- - - - - - - - - -

Quatrième Sprint 
================

Publication des sources
-----------------------
* Création du compte bitbucket;
* Création de la version git initiale du projet;
* Envoie de la version sur bitbucket;
* Autorisation en écriture pour l'utilisateur "jlaz".

Documentation du projet
-----------------------
* Création du fichier "README.md";
* Création des annexes.