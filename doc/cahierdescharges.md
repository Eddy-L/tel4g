﻿# Cahier des charges technique

1. Mise en forme
J'utiliserai l'**HTML** pour la mise en forme du site web.

2. Language côté serveur
Côté serveur, l'utilisation du **PHP** sera requise pour pouvoir dynamiser mes pages. Il me permettra entre autre de:
	* Créer du contenu dynamique;
	* Faire des connexions avec ma base de données;
	* Gérer des pages webs en fonction de chaques utilisateurs;
	* Etc...

3. Stockage de données
Pour gérer ma base de données, j'utiliserai **MySQL**. 

4. Styles
Le framework **Bootstrap** permettant de développer des applications en html, css et javascript sera d'une grande utiliter pour mettre en forme le site facilement. Il ne m'empechera pas non plus de créer mes propres styles **CSS**

5. Dynamisation
L'utilisation du **JQuery** (et/ou du **AJAX**) sera d'une grande utilité pour me faciliter l'utilisation des scripts *javascript*. Le javascript me permettra de dynamiser mes pages web pour que l'utilisateur final du site prenne plaisir à naviguer sur celui-ci. Il permettra néanmoins de créer des interactions avec le site sans pour autant recharger les pages.

6. Framework
En ce qui concerne le framework, j'utiliserai le micro-framework **CodeIgniter** pour sa simplicité d'apprentissage. Il offre le strict minimum comme fonctionnalité et permet également d'y ajouter du contenu optionnel.

7. Gestion des versions
Pour gérer au mieux l'évolution de site web lors de l'insertion de nouveau module, j'utiliserai **Git**.  Mais ici encore je ne suis pas sur de l'utiliser pour ce projet.

#Outils à utilisés
* HTML
* CSS
* Javascript/Jquery
* PHP
* MySQL
* Bootstrap
* CodeIgniter
* Git