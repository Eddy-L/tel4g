﻿Procédure d'installation
========================

Base de données
---------------
Création de la base de données "tel4g" sur mySQL: [tel4g](tel4g.sql)

Copie du projet
---------------
Grâce à la console git, se placer dans le répertoire www de wamp: 
	
	cd c:/wamp/www
	
Copier la ligne qui suit dans la console git:

	
	git clone https://Eddy-L@bitbucket.org/Eddy-L/tel4g.git
	
Local.tel4g
-----------	
Suivre la procédure pour utiliser l'adresse "local.tel4g": [documentation](hotevirtuel.md) 

Il ne vous reste plus qu'à vous rendre à l'adresse "local.tel4g" pour voir le résultat.