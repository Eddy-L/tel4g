﻿$(function(){
	$(".container1").on({
		click : function(){
			$(this).toggleClass("selectedOffer");
			$(".container_valid").css("display","block");
			var cptSelectAbonnements=$("#container_abonnements .selectedOffer").length;
			if(cptSelectAbonnements>1){
				$("#container_abonnements [id!="+$(this).attr('id')+"]").removeClass("selectedOffer");
				cptSelectAbonnements=$("#container_abonnements .selectedOffer").length;
			}
			
			if(cptSelectAbonnements==0){
				$(".container_valid").css("display","none");
				$(".container2").removeClass("selectedOffer");
				$(".container2").off("click").off("mouseover").off("mouseout");
			}else{
				$(".container2").off("click").off("mouseover").off("mouseout");
				$(".container2").on({
					"click" : function(){
						$(this).toggleClass("selectedOffer");
						var cptSelectOptions=$("#container_options .selectedOffer").length;
						if(cptSelectOptions>1){
							$("#container_options [id!="+$(this).attr('id')+"]").removeClass("selectedOffer");
						}
					},
					"mouseover" : function(){
						$(this).addClass("mouseOver");
					},
					"mouseout" : function(){
						$(this).removeClass("mouseOver");
					}
				});
			}
		},
		mouseover : function(){
			$(this).addClass("mouseOver");
		},
		mouseout : function(){
			$(this).removeClass("mouseOver");
		}
	});
	
	$("#bValider").click(function(event){
		// event.preventDefault();
		var idAbonnement=$("#container_abonnements .selectedOffer").attr('id').replace('offer','');
		$("#inputAbonnement").val(idAbonnement);
		
		if($("#container_options .selectedOffer").length){
			var idOption=$("#container_options .selectedOffer").attr('id').replace('offer','');
			$("#inputOption").val(idOption);		
		}
		
	});	
	
	if($(".selectedOffer").length){
		// alert("ok");
		$(".container_valid").css("display","block");
		$(".container2").on("click",function(){
						$(this).toggleClass("selectedOffer");
						var cptSelectOptions=$("#container_options .selectedOffer").length;
						if(cptSelectOptions>1){
							$("#container_options [id!="+$(this).attr('id')+"]").removeClass("selectedOffer");
						}
					});
	}

});

