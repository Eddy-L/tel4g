﻿$(function(){
	$('#navbar ul li').removeClass("active");
	$('#navbar ul #users').addClass("active");
	
	$("#inputDateNaissance").datepicker({
		showAnim: "slideDown",
		beforeShow: function(input, inst)
		{
			inst.dpDiv.css({marginTop: -input.offsetHeight + 'px', marginLeft: input.offsetWidth + 'px'});
		},
		changeMonth: true,
		changeYear: true
	});
});