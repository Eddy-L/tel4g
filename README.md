﻿INTRODUCTION
============

Pour le compte de la société tel4g, il m’a été demandé de mettre en place un site internet. J’ai donc eu recours à différents outils pour réussir ce projet :

 - **HTML** : Pour la mise en forme ;
 - **PHP** : Pour dynamiser les pages ;
 - **MySql** : Pour gérer ma base de données ;
 - **Bootstrap** & **CSS** : Pour une mise en forme plus agréable aux yeux ;
 - **Javascript** & **JQuery** : Pour dynamiser les pages sans rechargement de celles-ci ;
 - **CodeIgniter** : Framework PHP qui permet de se concentrer sur les principaux points du site et ne pas s’occuper des détails ;
 - **Git** : Pour gérer l’évolution du site.
- - - - - - - - - -

PAGE D’ACCUEIL
--------------
Lorsque vous vous trouvez sur la page d’accueil, vous avez à disposition les offres de téléphonie de l’entreprise. Il vous est possible de sélectionner l’abonnement de votre choix en cliquant simplement dessus. Une fois que vous avez choisi un abonnement, vous avez 2 possibilités :

 - Valider l’abonnement grâce au bouton qui est apparu ;
 - Sélectionner une option et valider ensuite votre abonnement.

*la page de validation sera expliqué plus tard*

Sur cette page, ainsi que sur toutes les autres, vous trouverez un bandeau de navigation en haut de la page qui vous permettra de vous rendre sur toutes les autres pages du site.

![Accueil](/assets/images/ImageDoc/accueil.png)
- - - - - - - - - -

BOUTIQUE DE TELEPHONES
----------------------
En arrivant sur la page de la boutique des téléphones, vous avez un aperçu de l’ensemble des téléphones vendus sur le site. Ces téléphones sont alors affichés avec :

 - Une image ; 
 - Nom et modèle ; 
 - Prix.
 
![Boutique téléphones](/assets/images/ImageDoc/phones.png)

Lorsque vous cliquez sur l’un d’eux, vous arrivez sur une page décrivant les différentes caractéristiques de celui que vous avez choisi. 

![Téléphone](/assets/images/ImageDoc/phone.png)

- - - - - - - - - -

INSCRIPTION
-----------

Cette page permet l’inscription des nouveaux utilisateurs sur le site. Elle reprend les informations personnelles de l’utilisateur :

 - Nom ; 
 - Prénom ; 
 - Date de naissance (Facultatif) ; 
 - Email ; 
 - Mot de passe (avec confirmation pour s’assurer que l’utilisateur n’a pas fait de faute de frappe).

L’adresse email sera alors utilisée comme identifiant pour l’utilisateur. Une adresse mail ne peut donc être utilisée qu’une seule fois à l’inscription et pour un seul utilisateur.
Si le formulaire d’inscription est correctement rempli, l’utilisateur est alors redirigé vers la page d’accueil et se retrouve automatiquement connecté au site.

![Inscription](/assets/images/ImageDoc/inscription.png)

- - - - - - - - - -

AUTHENTIFICATION
----------------

Cette page permet à un utilisateur, déjà enregistré sur le site, de se connecter grâce à son adresse email et à son mot de passe. S’il n’est pas encore inscrit, il peut toujours être redirigé vers la page d’inscription grâce à un simple lien en dessous du formulaire d’authentification.

![Authentification](/assets/images/ImageDoc/authentification.png)

- - - - - - - - - -

VALIDATION ABONNEMENT
---------------------

Cette page est accessible lorsque l’utilisateur a fait un choix d’un abonnement et d’une option (facultatif) sur la page d’accueil et a cliqué sur le bouton « valider ». Il est alors redirigé sur une page comprenant un récapitulatif de ses choix. 
Il lui est alors possible de souscrire son abonnement (**En construction**) ou d’annuler celui-ci. Dans ce dernier cas, il est redirigé vers la page d’accueil, comprenant les offres. Ses précédents choix sont alors sélectionnés à nouveau. 

![Validation](/assets/images/ImageDoc/validation.png)

ANNEXES
============
 - [Procédure d'installation](doc/installation.md) 
 - [Code SQL pour la base de données](doc/tel4gSQL.md) 
 - [Utilisation de l'adresse "local.tel4g"](doc/hotevirtuel.md) 
 - [Cahier des charges technique](doc/cahierdescharges.md) 
 - [Sprints](doc/sprints.md) 
