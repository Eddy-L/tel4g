﻿<form method="POST" action="">
	<table class="table" id="tableRecap">
		<caption class="center_class"><h1>Récapitulatif de votre abonnement</h1></caption>
		<tr>
			<th></th>
			<th>Produit</th>
			<th>Caractéristiques</th>
			<th>Tarif</th>
		</tr>
		<tr>
			<td><h3>Téléphonie</h3></td>
			<td><b><?php echo $abonnement[0]->nom_offre;?></b></td>
			<td><?php echo str_replace('--','</br>',$abonnement[0]->caract_offre);?></td>
			<td><b><?php echo $abonnement[0]->tarif_offre;?> €/mois</b></td>
		</tr>
		<tr>
			<td><h3>Options</h3></td>
			<td><b><?php echo (isset($option[0])? $option[0]->nom_offre:" - ");?></b></td>
			<td><?php echo (isset($option[0])? str_replace('--','</br>',$option[0]->caract_offre):" - ");?></td>
			<td><b><?php echo (isset($option[0])? $option[0]->tarif_offre." €/mois": " - ");?></b></td>
		</tr>
		<tr>
			<td><h3>Total</h3></td>
			<td></td>
			<td></td>
			<td><b><?php echo $total;?> €/mois</b></td>
		</tr>
		<tr>			
			<td colspan="4" class="center_class">
				<input type="submit" class="btn btn-custom" id="bSouscrire" name="bSouscrire" value="Souscrire">
				<input type="submit" class="btn btn-custom-cancel" id="bAnnuler" name="bAnnuler" value="Annuler">
			</td>
		</tr>
	</table>
</form>