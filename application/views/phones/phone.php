﻿<div class="row col-md-12">
	<div class="row">
		<div class="col-md-4 center_class">
			<div class="image_caracteristique">
				<?php echo img("phones/".$phone[0]->img_phone,$phone[0]->name_phone,"img-responsive-custom"); ?>
			</div>
			<div class="center_class">
				<h1><?php echo $phone[0]->prix_phone;?> €</h1>
			</div>
		</div>
		<div class="col-md-7">
			<div class="row">
				<h2><?php echo $phone[0]->name_brand . " " . $phone[0]->name_phone;?></h2>
			</div>
			<div class="row">
				<table class="table table-condensed">
					<tr>
						<th>Caractéristique</th>
						<th>Valeur</th>
					</tr>
					<tr>
						<td>Poids (g)</td>
						<td><?php echo $phone[0]->weight_phone;?></td>
					</tr>
					<tr>
						<td>Diagonale (pouces)</td>
						<td><?php echo $phone[0]->diag_phone;?></td>
					</tr>
					<tr>
						<td>Hauteur (mm)</td>
						<td><?php echo $phone[0]->height_phone;?></td>
					</tr>
					<tr>
						<td>Largeur (mm)</td>
						<td><?php echo $phone[0]->lenght_phone;?></td>
					</tr>
					<tr>
						<td>Epaisseur (mm)</td>
						<td><?php echo $phone[0]->thick_phone;?></td>
					</tr>
					<tr>
						<td>Autonomie (h)</td>
						<td><?php echo $phone[0]->autonomy_phone;?></td>
					</tr>
					<tr>
						<td>Système d'exploitation</td>
						<td><?php echo $phone[0]->nom_os;?></td>
					</tr>
					<tr>
						<td>Capacité mémoire (Go)</td>
						<td><?php echo $phone[0]->memory_phone;?></td>
					</tr>
					<tr>
						<td>Extension mémoire</td>
						<td><input type="checkbox" disabled <?php echo ($phone[0]->memory_ext_phone==1)?"checked":"";?>/></td>
					</tr>
					<tr>
						<td>Norme 4G</td>
						<td><input type="checkbox" disabled <?php echo ($phone[0]->four_g_phone==1)?"checked":"";?>/></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

	