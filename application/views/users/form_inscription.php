﻿<div>
	<h1 class="center_class">Inscription</h1></br>
	<form method="POST" action="" class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputNom">Nom*:</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="inputNom" name="inputNom" placeholder="Nom" value="<?php echo set_value("inputNom"); ?>">
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputNom'); ?></b>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputPrenom">Prénom*:</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="inputPrenom" name="inputPrenom" placeholder="Prénom" value="<?php echo set_value("inputPrenom"); ?>">
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputPrenom'); ?></b>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputDateNaissance">Date de naissance:</label>
			<div class="col-sm-3">
				<input type="date" class="form-control" id="inputDateNaissance" name="inputDateNaissance" placeholder="Date de naissance" value="<?php echo set_value("inputDateNaissance"); ?>" readonly>
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputDateNaissance'); ?></b>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputEmail">Email*:</label>
			<div class="col-sm-3">
				<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Adresse Email" value="<?php echo set_value("inputEmail"); ?>">
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputEmail'); ?></b>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputMdp">Mot de passe*:</label>
			<div class="col-sm-3">
				<input type="password" class="form-control" id="inputMdp" name="inputMdp" placeholder="Mot de passe">
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputMdp'); ?></b>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputMdp">Confirmation mot de passe*:</label>
			<div class="col-sm-3">
				<input type="password" class="form-control" id="inputConfirmMdp" name="inputConfirmMdp" placeholder="Confirmation">
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputConfirmMdp'); ?></b>
			</div>
		</div>
		<div class="form-group">        
			<div class="col-sm-offset-6 col-sm-6">
				<button type="submit" class="btn btn-default" name="bEnvoyer">M'inscrire</button>
			</div>
		</div>
	</form>
	</br></br></br>
</div>