﻿<div>
	<h1 class="center_class">Identification</h1></br>
	<form method="POST" action="" class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputEmail">Email*:</label>
			<div class="col-sm-3">
				<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Adresse Email" value="<?php echo set_value("inputEmail"); ?>">
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputEmail'); ?></b>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-6" for="inputMdp">Mot de passe*:</label>
			<div class="col-sm-3">
				<input type="password" class="form-control" id="inputMdp" name="inputMdp" placeholder="Mot de passe">
			</div>
			<div class="col-sm-3 text-danger">
				<b><?php echo form_error('inputMdp'); ?></b>
			</div>
		</div>
		<div class="form-group">        
			<div class="col-sm-offset-6 col-sm-6">
				<button type="submit" class="btn btn-default" name="bConnection">M'identifier</button>
			</div>
		</div>
	</form>
	<div class="col-sm-offset-6 col-sm-6"><a href="<?php echo base_url();?>user/new">Je souhaite m'inscrire</a></div>
</div>