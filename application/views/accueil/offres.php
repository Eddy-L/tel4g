﻿	<div id="container_abonnements" class="row col-lg-12">
		<h1>Abonnements</h1>
		<?php 
			$idAbonnement=$this->session->flashdata("idAbonnement");
			// echo $idAbonnement;
			foreach($abonnements as $abonnement): 
				$this->load->view('accueil/template_offre',$abonnement);
			endforeach; 
		?>
	</div>
	
	<div id="container_options"  class="row col-lg-12">
		<h1>Options</h1>
		<?php 
			$idOption=$this->session->flashdata("idOption");
			// echo $idOption;
			foreach($options as $option): 
				$this->load->view('accueil/template_offre',$option);
			endforeach; 
		?>
	</div>