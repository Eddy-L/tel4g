﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function __construct()
    {
        //  Obligatoire
        parent::__construct();
        
		$this->load->helper('assets');
        
    }
	
	public function abonnement()
	{		
		if(isset($_POST["bAnnuler"])){
			$this->session->set_flashdata("idAbonnement",$this->session->userdata("idAbonnement"));
			$this->session->set_flashdata("idOption",$this->session->userdata("idOption"));
			
			//VOIR SI IL FAUT SUPPRIMER VAR DE SESSION abonnement et option
			redirect();
		}
		
		if($this->input->post('inputAbonnement')){
			$this->session->set_userdata("idAbonnement",$this->input->post('inputAbonnement'));
			$this->session->set_userdata("idOption",$this->input->post('inputOption'));
		}
		
		if(!$this->session->userdata("userEmail")){
			//Faire passé une variable de session pour qu'on sache qu'on doit redirigé vers la page recap ensuite
			$this->session->set_userdata("newPath","subscription/new");
			redirect(base_url()."authentication");
		}
		
		if(!$this->session->userdata("idAbonnement")){
			redirect();
		}
		
		$this->load->model('offres');
		
		$data = array();
		$data['abonnement'] = $this->offres->get_offre($this->session->userdata("idAbonnement"));
		$data['option'] = $this->offres->get_offre($this->session->userdata("idOption"));
		$data['total']=$data['abonnement'][0]->tarif_offre;
		if(isset($data['option'][0])){
			$data['total']+=$data['option'][0]->tarif_offre;
		}
		
		// $data['idAbonnement']=$this->session->userdata("idAbonnement");
		// $data['idOption']=$this->session->userdata("idOption");
		
		//Récupérer information concernant l'abonnement et l'option pour les envoyé à la page "récap" et les afficher correctement
		
		$this->load->library('layout');
		$this->layout->set_titre("Votre abonnement");
		$this->layout->view("shop/recap",$data);			
	}
}
