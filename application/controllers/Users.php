﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	private $validEmail=False;
	
	public function __construct()
    {
        //  Obligatoire
        parent::__construct();
        
		$this->load->helper('assets');
        
    }
	
	public function inscription()
	{	
		if($this->session->userdata("userEmail")){
			redirect();
		}
		
		$this->load->library('layout');
		$this->load->library('form_validation');
		$this->layout->set_titre("Rejoignez-nous");
		$this->layout->ajouter_js("script_user");
		
		$configRules=array(
			array(
				"field"=>"inputNom",
				"label"=>"Nom",
				"rules"=>"trim|required|min_length[3]|max_length[50]|alpha_dash|encode_php_tags",
				"errors"=>array(
							"required" => "Le champs %s doit être rempli",
						)
			),
			array(
				"field"=>"inputPrenom",
				"label"=>"Prénom",
				"rules"=>"trim|required|min_length[3]|max_length[50]|alpha_dash|encode_php_tags",
				"errors"=>array(
							"required" => "Le champs %s doit être rempli",
						)
			),
			array(
				"field"=>"inputDateNaissance",
				"label"=>"Date de naissance",
				"rules"=>"trim|encode_php_tags",
			),
			array(
				"field"=>"inputEmail",
				"label"=>"E-mail",
				"rules"=>"trim|required|min_length[3]|max_length[70]|encode_php_tags|valid_email|is_unique[users.email_user]",
				"errors"=>array(
							"required" => "Le champs %s doit être rempli",
							"valid_email" => "L'adresse Email n'est pas valide",
							"is_unique" => "L'adresse Email est déjà utilisée",
						)
			),
			array(
				"field"=>"inputMdp",
				"label"=>"Mot de passe",
				"rules"=>"trim|required|min_length[3]|max_length[200]|alpha_dash|encode_php_tags|matches[inputConfirmMdp]",
				"errors"=>array(
							"required" => "Le champs %s doit être rempli",
							"matches" => "Les mots de passe ne sont pas identiques!",
						)
			),
			array(
				"field"=>"inputConfirmMdp",
				"label"=>"Confirmation mot de passe",
				"rules"=>"trim|min_length[3]|max_length[200]|alpha_dash|encode_php_tags",
				"errors"=>array(
							"required" => "Le champs %s doit être rempli",
						)
			),
		
		);
		$this->form_validation->set_rules($configRules);
		// $this->form_validation->set_rules('inputPrenom','Prénom','trim|required|min_length[3]|max_length[100]|alpha_dash|encode_php_tags| ');


		
		if ($this->form_validation->run())
		{
			$nom=$this->input->post('inputNom');
			$prenom=$this->input->post('inputPrenom');
			$date=$this->input->post('inputDateNaissance');
			$email=$this->input->post('inputEmail');
			$mdp=$this->input->post('inputMdp');
			
			
			$this->load->model('musers');
			$this->musers->add_user($nom,$prenom,$date,$email,$mdp);
			
			$this->session->set_userdata("userEmail",$this->input->post('inputEmail'));
			if($this->session->userdata("newPath")){
				$newPath=$this->session->userdata("newPath");
				$this->session->unset_userdata('newPath');
				redirect(base_url().$newPath);
			}
			else{
				redirect();

			}
			// $this->session->set_userdata("userEmail",$this->input->post('inputEmail'));
			// redirect(base_url()."accueil");
		}
		else
		{
			$this->layout->view("users/form_inscription");
		}
					
	}
	
	public function connection()
	{	
		if($this->session->userdata("userEmail")){
			redirect();
		}
		
		$this->load->model('musers');
		$this->load->library('layout');
		$this->load->library('form_validation');
		$this->layout->set_titre("Identifiez-vous");
		$this->layout->ajouter_js("script_user_connection");
		
		$configRules=array(
			array(
				"field"=>"inputEmail",
				"label"=>"E-mail",
				"rules"=>"trim|required|encode_php_tags|valid_email|callback_check_email",
				"errors"=>array(
							"required" => "Le champs %s doit être rempli",
							"valid_email" => "L'adresse Email n'est pas valide",
							"is_unique" => "L'adresse Email est déjà utilisée",
						)
			),
			array(
				"field"=>"inputMdp",
				"label"=>"Mot de passe",
				"rules"=>"trim|required|alpha_dash|encode_php_tags|callback_check_mdp",
				"errors"=>array(
							"required" => "Le champs %s doit être rempli",
							"matches" => "Les mots de passe est incorrecte!",
						)
			),		
		);
		$this->form_validation->set_rules($configRules);
		// $this->form_validation->set_rules('inputPrenom','Prénom','trim|required|min_length[3]|max_length[100]|alpha_dash|encode_php_tags| ');


		
		if ($this->form_validation->run())
		{
			$this->session->set_userdata("userEmail",$this->input->post('inputEmail'));
			if($this->session->userdata("newPath")){
				$newPath=$this->session->userdata("newPath");
				$this->session->unset_userdata('newPath');
				redirect(base_url().$newPath);
			}
			else{
				redirect();

			}
			//$this->layout->view("users/form_connection");
		}
		else
		{
			$this->layout->view("users/form_connection");
			
		}
					
	}
	
	public function check_email($str){
		
		if ($this->musers->check_email($str)==0)
		{
			$this->form_validation->set_message('check_email', "L'adresse mail n'existe pas");
			return FALSE;
		}
		else
		{
			$this->validEmail=True;
			return TRUE;
		}
	}
	
	public function check_mdp($str){
		
		
		//On affiche ce message seulement si l'email est valide et que le mot de passe n'est pas correcte
		if ($this->validEmail)
		{
			$mdp=$this->musers->keep_user($this->input->post('inputEmail'))[0]->mdp_user;
			if($mdp!=$str){
				$this->form_validation->set_message('check_mdp', 'Le %s est incorrecte');
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
	}
	
	public function deconnexion(){
		//	Détruit la session
		$this->session->sess_destroy();

		//	Redirige vers la page d'accueil
		redirect();
	}
}
