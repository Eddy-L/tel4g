﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Phones extends CI_Controller {

	public function __construct()
    {
        //  Obligatoire
        parent::__construct();
        
		$this->load->helper('assets');
        
    }
	
	public function index()
	{	
		$this->load->model('mphones');

		$data = array();

		$data['phones'] = $this->mphones->get_phones();
		
		
		$this->load->library('layout');
		$this->layout->set_titre("Nos téléphones");
		$this->layout->ajouter_js("script_phone");
	
		$this->layout->view("phones/phones_index",$data);			
	}
	
	public function get_phone($id){
		$this->load->model('mphones');

		$data = array();

		$data['phone'] = $this->mphones->get_phone($id);
		
		
		$this->load->library('layout');
		$this->layout->set_titre($data['phone'][0]->name_phone);
		$this->layout->ajouter_js("script_phone");
		
		$this->layout->view("phones/phone",$data);
	}
}
