﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {

	public function __construct()
    {
        //  Obligatoire
        parent::__construct();
        
		$this->load->helper('assets');
        
    }
	
	public function index()
	{	
		
		$this->load->model('offres');

		$data = array();

		$data['abonnements'] = $this->offres->get_abonnements();
		$data['options'] = $this->offres->get_options();
		
		
		$this->load->library('layout');
		$this->layout->set_titre("Nos offres");
		$this->layout->ajouter_js("script_accueil");
	
		/*$this->layout->views('premiere_vue')
		     ->views('deuxieme_vue')
		     ->view('derniere_vue');*/
		$this->layout->views("accueil/offres",$data)->view("accueil/form_valider");
	}
}
