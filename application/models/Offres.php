﻿<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offres extends CI_Model
{
    protected $table = 'offres';

	public function get_abonnements()
    {
        //type offre 1 correspond aux abonnements
		return $this->db->select('*')
			->from($this->table)
			->where('type_offre',1)
			->order_by('id_offre', 'desc')
			->get()
			->result();
    }
	
	public function get_options()
    {
        //type offre 2 correspond aux options
        return $this->db->select('*')
			->from($this->table)
			->where('type_offre',2)
			->order_by('id_offre', 'desc')
			->get()
			->result();
    }
	
	
	/* Retourne 1 enregistrement selon l'id*/
	public function get_offre($id)
	{
		return $this->db->select('*')
			->from($this->table)
			->where('id_offre',$id)
			->order_by('id_offre', 'desc')
			->get()
			->result();
	}
}