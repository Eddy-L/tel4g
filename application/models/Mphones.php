﻿<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MPhones extends CI_Model
{
    protected $table = 'phones';

	public function get_phones()
    {
        //type offre 1 correspond aux abonnements
		return $this->db->select('p.id_phone,p.name_phone,p.prix_phone,p.img_phone,pb.name_brand')
			->from($this->table." p")
			->join("phones_brand pb", "pb.id_brand = p.brand_phone")
			->order_by('p.name_phone', 'asc')
			->get()
			->result();
    }
	
	public function get_phone($id)
    {
        //type offre 2 correspond aux options
        return $this->db->select('*')
			->from($this->table." p")
			->join("phones_brand pb", "pb.id_brand = p.brand_phone")
			->join("phones_os o", "o.id_os = p.os_phone")
			->where('p.id_phone',$id)
			->get()
			->result();
    }
}