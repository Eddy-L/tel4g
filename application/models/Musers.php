﻿<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MUsers extends CI_Model
{
    protected $table = 'users';

	public function add_user($nom,$prenom,$date,$email,$mdp)
    {
        $this->db->set('nom_user',  $nom);
        $this->db->set('prenom_user',  $prenom);
        if($date!=""){
			$this->db->set('date_user',  date("Y-m-d", strtotime($date)));
		}
        $this->db->set('email_user',  $email);
        $this->db->set('mdp_user',  $mdp); //Mettre en md5 le mdp
		
		return $this->db->insert($this->table);
    }
	
	public function check_email($email){
		return (int) $this->db->from($this->table)
                        ->where("email_user",$email)
                        ->count_all_results();
	}
	
	public function keep_user($email){
		return $this->db->select('*')
					->from($this->table)
                    ->where("email_user",$email)
                    ->get()
					->result();
	}
}