﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" > 
	<head>
		<title><?php echo $titre; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>" />
		<?php foreach($css as $url): ?>
				<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
		<?php endforeach; ?>
		<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

	</head>

	<body>
		<div class="navbar navbar-inverse navbar-fixed-top">
		  <div class="container">
			
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li id="accueil" class="active"><a href="<?php echo base_url();?>index">Nos offres</a></li>
					<li id="phones"><a href="<?php echo base_url();?>phones">Nos téléphones</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						if(!$this->session->userdata("userEmail")){
					?>
							<li id="users"><a href="<?php echo base_url();?>user/new"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
							<li id="user_connection"><a href="<?php echo base_url();?>authentication"><span class="glyphicon glyphicon-log-in"></span> Connexion</a></li>
					<?php
						}
						else{
					?>
							<li id="user_logout"><a href="<?php echo base_url();?>users/deconnexion"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a></li>
					<?php
						}
					?>
					
				</ul>
			</div>
		  </div>
		</div>

		<div class="container">
			<?php 
				echo $output; 
			?>
		</div>
		
		
		
		<script src="<?php echo base_url();?>assets/javascript/jquery.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
		<?php foreach($js as $url): ?>
				<script type="text/javascript" src="<?php echo $url; ?>"></script> 
		<?php endforeach; ?>
	</body>

</html>